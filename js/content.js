$(function() {
  var selectors = {
    overTimeApplyButton: function () {
      return $("div.JfTitlePanel div.x-component div.x-btn-s button span")
        .filter(function () { return $(this).text() === "残業申請" })
        .parent();
    },

    overTimeDateInput: function () {
      return $("input[name=workDate]");
    },

    overTimeReasonInput: function () {
      return $("input[name=overReason]");
    },

    overTimeEndInput: function () {
      return $("input[name=overPlanTime]");
    },

    overTimeSubmitButton: function () {
      return $("div.x-window.editDialog button:contains('申請')");
    }
  };

  var awaitElements,
    wait,
    applyOvertime;

  wait = function(callback) {
    setTimeout(callback, 1000)
  };

  waitHalf = function(callback) {
    setTimeout(callback, 500);
  };

  awaitElements = function(selectors, callback) {
    var elems = _.map(selectors, function(s) { return s(); });

    if (!_.every(elems, function(elem) { return elem.length > 0 })) {
      //if (elem.length === 0) {
      setTimeout(function() {
        awaitElements(selectors, callback);
      }, 100);
    } else {
      console.log("appears!!");
      callback();
    }
  };

  applyOvertime = function(date, end, reason) {
    selectors.overTimeApplyButton().click();

    awaitElements([
      selectors.overTimeDateInput,
      selectors.overTimeEndInput,
      selectors.overTimeReasonInput
    ], function() {
      wait(function () {
        selectors.overTimeDateInput().click();

        waitHalf(function () {
          selectors.overTimeDateInput().val(date);
          selectors.overTimeDateInput().click();

          wait(function () {
            selectors.overTimeEndInput().click();

            waitHalf(function () {
              selectors.overTimeEndInput().val(end);
              selectors.overTimeEndInput().click();

              wait(function () {
                selectors.overTimeReasonInput().click();

                waitHalf(function () {
                  selectors.overTimeReasonInput().val(reason);
                  selectors.overTimeReasonInput().click();

                  //wait(function () {
                  //  selectors.overTimeSubmitButton().click();
                  //});
                })
              })
            });
          });
        });
      });
    });
  };

  chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
    //alert($("title").text());
    applyOvertime("2016/05/20", "24:00", "開発");
  });
});

console.log("content loaded");
